
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

int main(){
  int servSocket, newSocket;
  char buffer[24];
  struct sockaddr_in serverAddr;
  struct sockaddr_storage serverStorage;
  socklen_t addr_size;

    //    tworzymy sockety z trzeba argumentami
  servSocket = socket(PF_INET, SOCK_STREAM, 0);
  
    //    konfigurowanie opcji adresu servera (struct)
  serverAddr.sin_family = AF_INET;
  
    //    ustawianie numeru portu przez  htons
  serverAddr.sin_port = htons(7891);
  
    //    IP na localhost
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  
    //    ustawienie bitów 0
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

    //    powiązanie adresu struktury z socketem
  bind(servSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));

    //    Nasłuchiwanie (socket) max 5 połączeń
  if(listen(servSocket,5)==0)
    printf("Star serwera.... nasłuchiwanie\n");
  else
    printf("Error\n");

    //    nowy socket dla połączeń przychodzących, przyjmowanie połaczeń
  addr_size = sizeof serverStorage;
  newSocket = accept(servSocket, (struct sockaddr *) &serverStorage, &addr_size);

    //    powitanie dla nowego socketu połączeń
  strcpy(buffer,"Hello World\n");
  send(newSocket,buffer,24,0);
  printf("Wysłano komunikat");  

  return 0;
}